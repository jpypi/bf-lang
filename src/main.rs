use std::{fs::File, io::{BufReader, Read}, path::PathBuf};

use clap::Parser;

use bf::lang::*;


fn help() -> &'static str {
    "Language Spec:

    >  INC DP
    <  DEC DP
    +  INC Byte @ DP
    -  DEC Byte @ DP
    .  output byte at DP
    ,  input 1 byte to DP
    [  if byte at DP == 0 jmp to command after matching ]
    ]  if byte at DP != 0 jmp back to command after maching [

Examples:

    // Hello World!
    ++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.

    // 2 + 2 # Destructive
    ++>++
    <[->+<]

    // Copy 2 to next two cells
    ++
    [->+>+<<]

    // Shift data value to left
    >++
    [-<+>]

    // Copy 2 to next cell using a 3rd cell
    ++
    [->+>+<<]
    >[-<+>]
    >[-<+>]

    // Multiply 3 * 4 using 5 data tape cells
    // a: Mult loop
    // b: Add loop
    // c: Add quantity
    // d: Intermediate copy location
    // e: Result
    +++>++++       Initialize data values: 3 and 4
    <[
      -            Subtract from mult loop
      >[->+>+<<]   Copy b to c and d
      >[-<+>]      Move c back to b
      >[->+<]      Add d to e
      <<<          Return back to a
    ]"
}


#[derive(Parser, Debug)]
#[command(version, about = "Simple brainfuck interpreter", after_help = help())]
struct Args {
    #[arg(help="File to read source code from")]
    filename: Option<PathBuf>,

    #[arg(long, short, default_value_t = false, help="Enable output of the data tape, data pointer, and program counter")]
    debug: bool,

    #[arg(short, default_value_t = false, help="Do not print the trailing newline character")]
    newline_supress: bool,
}


fn main() {
    let args = Args::parse();
    // TODO: Make sure this works right
    // let source = "+[-->-[>>+>-----<<]<--<---]>-.";//>>>+.>>..+++[.>]<<<<.+++.------.<<-.>>>>+.";

    let mut source = String::new();

    if let Some(path) = args.filename {
        if let Ok(f) = File::open(&path) {
            let mut reader = BufReader::new(f);
            if reader.read_to_string(&mut source).is_err() {
                println!("Unable to read file contents.");
                return;
            }
        } else {
            println!("Unable to open file: {}", path.to_str().unwrap_or("BAD FILENAME"));
            return;
        }
    } else if std::io::stdin().read_to_string(&mut source).is_err() {
        println!("Unable to read from stdin.");
        return;
    }

    let tokens = lex(&source);
    match parse(&source, tokens) {
        Ok(program) => { execute(&program, std::io::stdin(), std::io::stdout().lock(), args.debug, None).unwrap(); }
        Err(e) => { println!("{}", e); },
    };

    if !args.newline_supress {
        println!();
    }
}