use std::{collections::HashMap, io::{Read, Write}};


#[derive(Debug, Clone, Copy)]
pub enum Token {
    IncDp,
    DecDp,
    Inc,
    Dec,
    Out,
    In,
    Start,
    End,
}


#[derive(Debug, Clone, Copy)]
pub enum Instruction {
    IncDp,
    DecDp,
    Inc,
    Dec,
    Out,
    In,
    Start(usize),
    End(usize),
}


pub fn lex(program: &str) -> Vec<Token> {
    program.bytes().filter_map(|b|
        match b {
            b'>' => Some(Token::IncDp),
            b'<' => Some(Token::DecDp),
            b'+' => Some(Token::Inc),
            b'-' => Some(Token::Dec),
            b'.' => Some(Token::Out),
            b',' => Some(Token::In),
            b'[' => Some(Token::Start),
            b']' => Some(Token::End),
            _ => None,
        }
    ).collect()
}


pub fn parse(raw_program: &str, token_stream: Vec<Token>) -> Result<Vec<Instruction>, String> {
    let mut stack = Vec::new();
    let mut program = Vec::new();

    for (i, t) in token_stream.iter().enumerate() {
        let next_sym = match t {
            Token::Start => {
                stack.push(i);
                Instruction::Start(0)
            },
            Token::End   => {
                if let Some(e) = stack.pop() {
                    program[e] = Instruction::Start(i + 1);
                    Instruction::End(e + 1)
                } else {
                    return Err(format!("\n\t{}\n\t{}^\nSyntax error - found unexpected `]` at {}", raw_program, " ".repeat(i), i));
                }
            }
            Token::IncDp => Instruction::IncDp,
            Token::DecDp => Instruction::DecDp,
            Token::Inc   => Instruction::Inc  ,
            Token::Dec   => Instruction::Dec  ,
            Token::Out   => Instruction::Out  ,
            Token::In    => Instruction::In   ,
        };

        program.push(next_sym);
    }

    // Un-closed
    if !stack.is_empty() {
        return Err("\nSyntax error - Unclosed `[`".to_string());
    }

    Ok(program)
}


/// Executes a parsed brainfuck program
///
/// # Arguments
///
/// * `program` - The compiled brainfuck instructions
/// * `input_stream` - Stream which the program will read from
/// * `output_stream` - Stream which the program will write to (additionally, where the debug info will go)
/// * `debug_output` - Controls wether data tape, program counter, and data pointer are printed out at the end of execution
/// * `max_instructions` - Maximum number of instructions to execute (can prevent infinite loops)
pub fn execute(program: &Vec<Instruction>, mut input_stream: impl Read, mut output_stream: impl Write, debug_output: bool, max_instructions: Option<u32>) -> Result<HashMap<i32, i32>, String> {
    let mut data_pointer = 0;
    let mut data = HashMap::new();
    let mut program_counter = 0;
    let mut cycles = 0;

    while program_counter < program.len() && max_instructions.map_or(true, |x| cycles < x) {
        let s = program.get(program_counter).unwrap_or_else(|| panic!("Program error at {}", program_counter));

        match s {
            Instruction::IncDp => data_pointer += 1,
            Instruction::DecDp => data_pointer -= 1,
            Instruction::Inc => {
                let value = data.get(&data_pointer).unwrap_or(&0);
                data.insert(data_pointer, value + 1);
            },
            Instruction::Dec => {
                let value = data.get(&data_pointer).unwrap_or(&0);
                data.insert(data_pointer, value - 1);
            },
            Instruction::Out => {
                write!(output_stream, "{}", char::from_u32(*data.get(&data_pointer).unwrap_or(&0) as u32).unwrap_or('`')).map_err(|e| e.to_string())?;
            },
            Instruction::In => {
                let mut buf: [u8;1] = [0];
                input_stream.read_exact(&mut buf).map_err(|e| e.to_string())?;
                data.insert(data_pointer, buf[0] as i32);
            },
            Instruction::Start(e) => {
                if *data.get(&data_pointer).unwrap_or(&0) == 0 {
                    program_counter = *e;
                    continue;
                }
            },
            Instruction::End(s) => {
                if *data.get(&data_pointer).unwrap_or(&0) != 0 {
                    program_counter = *s;
                    continue;
                }
            },
        };

        program_counter += 1;
        cycles += 1;
    }

    if debug_output {
        let mut rendered = vec![];
        for i in 0..data.len() {
            rendered.push(data.get(&(i as i32)).expect("Should always be able to read from data tape"));
        }
        write!(output_stream, "\ncycles: {}\nprogram counter: {}\ndata pointer: {}\ndata: {:?}", cycles, program_counter, data_pointer, rendered).map_err(|e| e.to_string())?;
    }

    Ok(data)
}


/// Utility function which lexes, parses, and then executes a brainfuck program directly from source
///
/// # Arguments
///
/// * `program` - A string representing brainfuck source code
/// * `input_stream` - Stream which the program will read from
/// * `output_stream` - Stream which the program will write to (additionally, where the debug info will go)
/// * `debug_output` - Controls wether data tape, program counter, and data pointer are printed out at the end of execution
/// * `max_instructions` - Maximum number of instructions to execute (can prevent infinite loops)
pub fn run(program: &str, input_stream: impl Read, output_stream: impl Write, debug_output: bool, max_instructions: Option<u32>) -> Result<(), String> {
    let tokens = lex(program);
    let instructions = parse(program, tokens)?;
    execute(&instructions, input_stream, output_stream, debug_output, max_instructions)?;

    Ok(())
}